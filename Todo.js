import React from "react";
import firebase from "@react-native-firebase/app";
import "@react-native-firebase/firestore";
import { List } from "react-native-paper";

function Todo({ id, title, complete }) {
  async function Complete() {
    await firebase
      .firestore()
      .collection("todos")
      .doc(id)
      .update({
        complete: !complete
      });
  }
  async function delTodo(id) {
    firebase
      .firestore()
      .collection("todos")
      .doc(id)
      .delete();
  }

  return (
    <List.Item
      title={title}
      onPress={() => delTodo(id)}
      left={props => (
        <List.Icon {...props} icon={complete ? "check" : "cancel"} />
      )}
    />
  );
}
export default React.memo(Todo);
